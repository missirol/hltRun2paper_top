
The increased instantaneous luminosity delivered by the LHC in 2017 and 2018
required to raise the online $p_{T}$ threshold of the most inclusive
unprescaled single-electron trigger, in order to limit its bandwidth.
%
This online $p_{T}$ threshold was set to 27 GeV in 2016,
and was raised to 32 GeV in 2017 and 2018.
%
This motivated the introduction of two electron+jets cross-triggers,
namely an electron+$H_{T}$ trigger and an electron+1-jet trigger,
both using an online $p_{T}$ threshold lower than 32 GeV.
%
These cross-triggers were designed to complement the single-electron trigger
in selected physics analyses targeting ${e+\text{jets}}$ final states;
these include, for example, measurements targeting
processes characterized by top quark production,
including the associated production of top quarks
with massive particles decaying to hadrons,
e.g. ${t\bar{t}H(\rightarrow b\bar{b})}$.
%
Differential measurements and searches for rare processes in the top sector
profit from not raising the offline cut on the electron $p_{T}$ above 32 GeV,
so the use of the logical OR of a given cross-trigger
with the single-electron trigger helps to improve the trigger efficiency for these analyses.
%
Both cross-triggers were unprescaled for the entirety
of the 2017 and 2018 $pp$ data-taking periods,
and they were seeded by a set of dedicated
%${e+\text{jets}}$
cross-triggers in the L1-Trigger {\color{red} \Large [Ref to TRG-17-001]}.

%The performance of the logical OR of each of these cross-triggers with the single-electron trigger
%is reported in terms of L1+HLT efficiencies, thus incorporating the efficiency of the corresponding L1-Trigger seeds.

%Monte Carlo (MC) simulation: $t\bar{t}$ production at QCD-NLO (\textsc{Powheg}+\textsc{Pythia8}).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The electron+$H_{T}$ trigger requires
an online electron with ${p_{T}>28~\text{GeV}}$ and ${|\eta|<2.1}$ passing
identification and isolation requirements, and ${H_{T}>150~\text{GeV}}$,
where $H_{T}$ corresponds to the scalar $p_{T}$-sum
of reconstructed online jets with ${p_{T}>30~\text{GeV}}$ and ${|\eta|<2.5}$.
The trigger is seeded by a dedicated set of
electron+$H_{T}$ cross-triggers in the L1-Trigger.
%
Given its high $H_{T}$ threshold,
this cross-trigger is best suited for ${e+\text{jets}}$ final states
with a large number of reconstructed jets;
processes leading to such final states include, for example,
$t\bar{t}H$ production with the Higgs boson decaying to a $b\bar{b}$ pair.
%
The efficiency measurement is performed in a sample of $e\mu$ events
collected by an independent single-muon trigger.
%
Events are selected offline requiring the presence of
a muon firing the single-muon trigger,
a reconstructed electron with ${p^{e}_{T} > 30~\text{GeV}}$ and ${|\eta^{e}|<2.5}$,
and at least four jets with ${p^{j}_{T} > 30~\text{GeV}}$ and ${|\eta^{j}|<2.4}$;
both charged leptons are required to pass
tight identification and isolation requirements.
%
Figure~\ref{fig:EleHT} (left)
shows a comparison of the efficiencies of
the single-electron trigger and
the logical OR of the latter with the electron+$H_{T}$ cross-trigger
in 2017 data.
%
The introduction of the cross-trigger improves
the overall efficiency from $79\%$ (single-electron trigger) to $83\%$.
%
The efficiency gain is concentrated
in the lower end of the electron $p_{T}$ spectrum,
due to the looser online $p_{T}$ threshold applied in the cross-trigger.
%
Figure~\ref{fig:EleHT} (right)
shows the performance of the OR of the electron+$H_{T}$ and single-electron triggers
in 2017 and 2018 data and simulation.
%
The lower efficiency observed in 2017
is due to detector-related issues
affecting part of the 2017 data-taking period
(failure of DCDC converters in the CMS pixel system).

\begin{figure}\centering
%\vspace*{+0.15cm}
%\hspace*{-0.00cm}
\includegraphics[scale=0.375]{figures/TOP_ElectronJets/ele_pt_ele28_ht150_OR_ele32_vs_ele32_data_2017.pdf}
%\hspace*{+0.25cm}
%\includegraphics[scale=0.375]{figures/TOP_ElectronJets/njets_ele28_ht150_OR_ele32_vs_ele32_data_2017.pdf}
\includegraphics[scale=0.375]{figures/TOP_ElectronJets/njets_ele28_ht150_OR_ele32_datamc_2017_2018.pdf}
\caption{%
Left:
L1+HLT efficiencies of
the single-electron trigger and
the logical OR of the single-electron and electron+$H_{T}$ triggers
in 2017 data,
as a function of the offline electron $p_{T}$.
The cut on the electron $p_{T}$,
indicated by the gray dashed line,
is loosened in order to show the turn-on
of the trigger efficiencies.
%
Right:
L1+HLT efficiencies of the logical OR of
the single-electron and electron+$H_{T}$ triggers
in 2017 and 2018 data and simulation,
as a function of the number of jets reconstructed offline.}
\label{fig:EleHT}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The electron+1-jet cross-trigger requires
an online electron with ${p_{T}>30~\text{GeV}}$ and ${|\eta|<2.1}$ passing
identification and isolation requirements, and
one online jet with ${p_{T}>35~\text{GeV}}$ and ${|\eta|<2.6}$,
not overlapping with the triggering electron (${\Delta R > 0.4}$).
%
The L1 seeds of this trigger implement a similar selection,
requiring the presence of an L1-electron and an L1-jet
with a minimum $\Delta R$-separation of $0.3$.
%
This cross-trigger is designed for
semileptonic final states with low jet multiplicity,
like those originating, for example, from single-top production.
%
Similarly to the case of the electron+$H_{T}$ trigger,
the trigger efficiency is measured
in a sample of $e\mu$ events
collected with a single-muon trigger.
%
Offline events are required to include
a muon firing the single-muon trigger,
a reconstructed electron with ${p^{e}_{T} > 32~\text{GeV}}$ and ${|\eta^{e}|<2.5}$,
and at least one jet with ${p^{j}_{T} > 40~\text{GeV}}$ and ${|\eta^{j}|<2.4}$;
both charged leptons are required to pass
tight identification and isolation requirements.
%
Figure~\ref{fig:EleJet} (left)
compares the efficiencies of
the single-electron trigger and
its logical OR with the electron+1-jet cross-trigger
in 2018 data.
%
The introduction of the cross-trigger improves
the overall efficiency by approximately $2\%$
%from $86\%$ (single-electron trigger) to $88\%$ in data.
and %,
%
%similarly to the electron+$H_{T}$ cross-trigger,
the efficiency gain is concentrated
in the electron low-$p_{T}$ region.
%
Figure~\ref{fig:EleJet} (right)
shows the efficiency of the logical OR of the single-electron and electron+1-jet triggers
in 2017 and 2018 data and simulation.
The lower efficiency observed in 2017
is due to the issues affecting the CMS pixel system
for part of the 2017 data-taking period.

\begin{figure}
\centering
%\vspace*{+0.15cm}
%\hspace*{-0.00cm}
\includegraphics[scale=0.375]{figures/TOP_ElectronJets/ele_pt_ele30_jet35_OR_ele32_vs_ele32_data_2018.pdf}
%\hspace*{+0.25cm}
%\includegraphics[scale=0.375]{figures/TOP_ElectronJets/ele_pt_ele30_jet35_OR_ele32_datamc_2018.pdf}
\includegraphics[scale=0.375]{figures/TOP_ElectronJets/ele_pt_ele30_jet35_OR_ele32_datamc_2017_2018.pdf}
\caption{%
Left:
L1+HLT efficiencies of the single-electron trigger and
the logical OR of the single-electron and electron+1-jet triggers
in 2018 data,
as a function of the offline electron $p_{T}$.
The cut on the electron $p_{T}$,
indicated by the gray dashed line,
is loosened in order to show the turn-on
of the trigger efficiencies.
%
Right:
L1+HLT efficiencies of the logical OR of
the single-electron and electron+1-jet triggers
in 2017 and 2018 data and simulation,
as a function of the offline electron $p_{T}$.}
\label{fig:EleJet}
\end{figure}
