
Performance of selected cross-triggers for Top physics analyses in the LHC Run-2:

 - Detector Performance Note: [DP-2019/026](http://cms.cern.ch/iCMS/jsp/db_notes/notestable1.jsp?CMSNoteID=DP-2019/026)
 - TWiki: [HLTtopAllRun2](https://twiki.cern.ch/twiki/bin/view/CMSPublic/HLTtopAllRun2)

Create the `main.pdf` document by running the `compile.sh` script.
